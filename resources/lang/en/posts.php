<?php


return [
    'created_successfully' => 'Post created successfully.',
    'get_data' => 'Post fetched successfully.',
    'get_all_data' => 'Posts fetched successfully.',
    'update_post'=>'Post updated successfully',
    'delete_data' => 'Post daleted successfuly'
];
