<?php

namespace Database\Factories;

use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;
use Faker\Generator as Faker;

class UserFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = User::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'id' => $this->faker->uuid,
            'name' => $this->faker->name,
            'email' => $this->faker->unique()->safeEmail,
            'password' => $this->faker->password(),
            'userName' => $this->faker->unique()->userName,
            'active' => $this->faker->boolean(),
            'phone_number'=> $this->faker->unique()->phoneNumber(),
            'last_login_at' => now(),
            'intro' => $this->faker->sentence(),
            'city' => $this->faker->city(),
                ];
    }
}
