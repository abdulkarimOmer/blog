<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Traits\Uuids;

class Category extends Model
{
    use HasFactory,Uuids;


    protected $table = 'categories';
    protected $keyType = 'string';

    public $incrementing = false;


    protected $fillable = [
        'title',
        'slug',
        'context'
    ];

    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at',
    ];



}
