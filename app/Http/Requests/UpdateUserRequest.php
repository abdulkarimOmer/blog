<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdateUserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [

            'name' => ['string' ],
            'email' => ['string', 'email',  'unique:users',],
            'userName' => ['string', 'unique:users,userName,'],
            'password' => ['min:7', 'max:24'],
            'phone_number' => [ 'unique:users,phone_number'],
            'active' => ['boolean'],
            'last_login_at' => ['date'],
            'intro' => ['string'],
            'profile' => ['string'],
            'city' => ['string'],
        ];
    }
}
