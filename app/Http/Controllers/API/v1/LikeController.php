<?php

namespace App\Http\Controllers\API\v1;

use App\Http\Controllers\Controller;
use App\Http\Requests\LikeRequest;
use App\Http\Resources\LikeResource;
use App\Models\Like;
use App\Models\Post;
use Auth;
use DB;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class LikeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    public function likePost(Request $request, $post)
    {
        //request()->user()->posts()->with('')->orderBy()->get();
        $like = Like::make()->toArray();
        $like['id'] = Str::uuid();
        $like['user_id'] = request()->user()->id;
        $like['like'] = true;
        $like['likeable_id'] = $post;
        $like['likeable_type'] = Post::class;

        DB::table('likes')->insert($like);
        return sendResponse(__('posts.created_successfully'), LikeResource::make($like));
    }

    public function unlikePost(Request $request, $post)
    {
        //request()
        //request()->user()->posts()->with('')->orderBy()->get();
        //$value= DB::update("UPDATE likes SET like = 1 WHERE user_id ='{$user_id}' and likeable_id = '{$post}'");
        $value = DB::table('likes')->where('user_id', '=', request()->user()->id)->where('likeable_id', '=', $post)->update(['like' => false]);
        return $value;
    }

    public function likeComment(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Like  $like
     * @return \Illuminate\Http\Response
     */
    public function show(Like $like)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Like  $like
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Like $like)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Like  $like
     * @return \Illuminate\Http\Response
     */
    public function destroy(Like $like)
    {
        //
    }
}
